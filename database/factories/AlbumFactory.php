<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Album::class, function (Faker $faker) {
    return [
        'profile_id' => App\Models\Profile::where('type', 'artist')->inRandomOrder()->first()->id,
        'producer_id' => App\Models\Profile::where('type', 'producer')->inRandomOrder()->first()->id,
        'title' => $faker->name,
        'slug' => str_slug($faker->unique()->name),
        'picture' => 'albums/onlygodjpg.jpg',
        'description' => $faker->sentences(50, true),
        'type' => ['song', 'video'][random_int(0, 1)],
        'release_date' => $faker->date,
        'is_promoted' => ['1', '0'][random_int(0, 1)],
    ];
});
