<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Profile::class, function (Faker $faker) {

    return [
        'genre_id' => App\Models\Genre::all()->random()->id,
        'name' => $faker->name,
        'stage_name' => $faker->unique()->name,
        'slug' => str_slug($faker->unique()->name),
        'is_sign' => ['1', '0'][random_int(0, 1)],
        'phone' => $faker->phoneNumber,
        'dob' => $faker->date,
        'email' => $faker->email,
        'location' => $faker->address,
        'label_content' => $faker->sentences(50, true),
        'picture' => 'profiles/Dee-1.jpg',
        'logo' => $faker->imageUrl(),
        'banner' => $faker->imageUrl(),
        'content' => $faker->sentences(50, true),
        'manager_content' => $faker->name,
        'gender' => ['male', 'female', 'unknown'][random_int(0, 2)],
        'type' => ['artist', 'producer', 'brand'][random_int(0, 2)],
        'facebook' => $faker->url,
        'instagram' => $faker->url,
        'twitter' => $faker->url,
        'is_promoted' => ['1', '0'][random_int(0, 1)],
    ];
});
