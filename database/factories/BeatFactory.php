<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Beat::class, function (Faker $faker) {
    return [
        'profile_id' => App\Models\Profile::where('type', 'producer')->inRandomOrder()->first()->id,
        'genre_id' => App\Models\Genre::all()->random()->id,
        'title' => $faker->name,
        'code' => str_random(10),
        'price' => random_int(0, 50),
        'bpm' => random_int(200, 900),
        'description' => $faker->sentences(50, true),
        'picture' => 'beats/beat-art.jpg',
        'preview' => 'beats/previews/live-ur-life.mp3',
    ];
});
