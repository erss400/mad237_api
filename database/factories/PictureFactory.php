<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Picture::class, function (Faker $faker) {

    return [
        'product_id' => App\Models\Product::all()->random()->id,
        'picture' => 'products/pictures/test-product-picture.jpg',
    ];
});
