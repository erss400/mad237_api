<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id', false, true);
            $table->integer('genre_id', false, true);
            $table->integer('producer_id', false, true)->nullable();
            $table->integer('album_id', false, true)->nullable();
            $table->string('title', 255)->unique();
            $table->text('description');
            $table->string('picture', 255);
            $table->string('preview', 255)->nullable();
            $table->string('file', 255);
            $table->string('feature', 255)->nullable();
            $table->string('release_date', 255)->nullable();
            $table->string('views', 355)->nullable();
            $table->boolean('is_promoted')->default(0);
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
            $table->foreign('producer_id')->references('id')->on('profiles')->onDelete('cascade');
            $table->foreign('genre_id')->references('id')->on('genres');
            $table->foreign('album_id')->references('id')->on('albums');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('songs');
        Schema::enableForeignKeyConstraints(); 
    }
}
