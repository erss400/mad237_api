<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id', false, true);
            $table->string('title');
            $table->string('slug');
            $table->string('sizes');
            $table->string('colors');
            $table->string('code');
            $table->string('price');
            $table->string('picture');
            $table->string('inventory')->default(0);
            $table->boolean('can_ship')->default(0);
            $table->text('description');
            $table->text('distribution_center');
            $table->string('tags')->nullable();
            $table->string('views', 355)->nullable();
            $table->boolean('is_promoted')->default(0);
            $table->string('discount')->nullable();
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
