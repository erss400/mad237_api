<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('genre_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('stage_name')->unique()->nullable();
            $table->string('slug')->unique();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('location')->unique()->nullable();
            $table->string('dob')->nullable();
            $table->enum('gender', ['male', 'female', 'unknown'])->default('unknown')->nullable();
            $table->text('content');
            $table->string('picture', 355)->nullable();
            $table->string('logo', 355)->nullable();
            $table->string('banner', 355)->nullable();
            $table->text('manager_content')->nullable();
            $table->text('booking')->nullable();
            $table->boolean('is_sign')->default(0)->nullable();
            $table->text('label_content')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();
            $table->string('views', 355)->nullable();
            $table->boolean('is_promoted')->default(0);
            $table->enum('type', ['artist', 'producer', 'brand'])->default('artist');
            $table->timestamps();
            
            $table->foreign('genre_id')->references('id')->on('genres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('profiles');
        Schema::enableForeignKeyConstraints(); 
    }
}
