<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id', false, true)->nullable();
            $table->integer('state_id', false, true)->nullable();
            $table->integer('city_id', false, true)->nullable();
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone_number')->unique()->nullable();
            $table->boolean('remember_me')->default(0)->nullable();
            $table->string('date_of_birth')->nullable();
            $table->enum('gender', ['male', 'female', 'unknown'])->default('unknown')->nullable();
            $table->boolean('is_verified')->default(0)->nullable();
            $table->boolean('status')->default(0)->nullable();
            $table->string('verification_code')->unique()->nullable();
            $table->text('bio')->nullable();
            $table->string('avatar')->nullable();
            $table->string('url')->unique()->nullable();
            $table->timestamps();

            $table->foreign('country_id')
                ->references('id')
                ->on('countries');

            $table->foreign('state_id')
                ->references('id')
                ->on('states');

            $table->foreign('city_id')
                ->references('id')
                ->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
    }
}
