@component('mail::message')
# Welcome

Thanks for signing up **we really appreciate** it. Click on the button below to activate your account or copy the **link** and paste in your browser.

@component('mail::button', ['url' => $user->url])
Verify My Account
@endcomponent

OR <br />

{{   $user->url }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
