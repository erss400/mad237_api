<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Song;
use App\Models\Album;
use App\Models\Video;
use App\Models\Genre;
use App\Models\Product;
use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\GenreTransformer;
use App\Transformers\ProfileTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
// use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{

    function __construct()
    {
        
    }

    public function get_profiles(Request $request)
    {
        if ($request->exists('genre_id')) {
            if (!empty($request->genre_id)) {
                $paginator = Profile::where('genre_id', $request->genre_id)->where('type', 'artist')->orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
            } else {
                $paginator = Profile::where('type', 'artist')->orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
            }
        } elseif ($request->exists('type')) {
            $paginator = Profile::where('type', $request->type)->orderBy('created_at', 'desc')->pagination(($request->has('pagination')) ? $request->pagination : 20);
        } else {
            $paginator = Profile::where('type', 'artist')->orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
        }
        

        return $this->response->paginator($paginator, new ProfileTransformer);
    }

    public function get_profile_by_slug($slug)
    {
        $data = Profile::where('slug', $slug)->first();

        if (!$data) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Profile Not Found');
        }

        if ($data->type == 'artist') {
            return $this->response->item($data, (new ProfileTransformer)->setDefaultIncludes(['photos', 'genre', 'albums', 'songs', 'videos', 'related']));
        } elseif ($data->type == 'brand') {
           return $this->response->item($data, new ProfileTransformer);
        }
    }

    public function get_genres()
    {
        $data = Genre::all();

        return $this->response->collection($data, new GenreTransformer);
    }
}
