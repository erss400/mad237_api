<?php

namespace App\Http\Controllers\Api\V1;

use Auth;
use Storage;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Like;
use App\Models\Album;
use App\Models\Artist;
use App\Models\Review;
use App\Models\Playlist;
use App\Models\Following;
use App\Events\SignUpEvent;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use App\Transformers\AlbumTransformer;
use App\Transformers\PlaylistTransformer;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['register', 'login', 'verify']]);
    }

    public function register(Request $request)
    {
        // return $this->success(env('EXPIRES_IN'), 200);
         $this->validate($request, [
            'name' => 'bail|required|string|min:6',
            'password' => 'bail|required|string|min:6',
            'username' => 'bail|required|unique:users',
            'email' => 'bail|required|email|unique:users'
        ]);

        $user = User::create([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'active' => 0,
        ]);

        if (!$user) {
            return $this->error('User not created', 302);
        }


        //generate user activation token and save
        $user->verification_code = mt_rand(193721, 666666);
        $user->url = 'http://localhost:8080/account/verify?code=' . $user->verification_code;
        $user->save();

        return $this->success('account created successfully, please check your email to activate.');
    }

    public function verify(Request $request, $code='')
    {
        if (empty($code)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No verification code found');
        }

        $user = User::where('verification_code', $code)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No account found to match verification code');
        }

        $user->verification_code = null;

        $user->is_verified = 1;

        if (!$user->save()) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to verify user account');
        }

        return $this->success('User account has been verified, login to update your account settings', 200);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'bail|required|string',
            'password' => 'bail|required|min:6'
        ]);

        if ($this->is_email($request->username)) {
            $user = User::where('email', $request->username)->first();

            if (!$user) {
                throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User account NOT found');
            }

            if (!$user->is_verified) {
                throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Account has not been verified');
            }

            $credentials['password'] = $request->input('password');
            $credentials['email'] = $request->input('username');

            if (! $token = JWTAuth::attempt($credentials)) {
                throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Wrong credentials');
            }

            return $this->respondWithToken($token);
        }

        $user = User::where('username', $request->username)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User account NOT found');
        }

        if (!$user->is_verified) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Account has not been verified');
        }

        $credentials = $request->all();

        if (! $token = JWTAuth::attempt($credentials)) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Wrong credentials');
        }

        return $this->respondWithToken($token);
    }

    /**
     * forgot password
     *
     * @param  object Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgot_password(Request $request)
    {
        $this->validate($request, [
            'username' => 'bail|required|string'
        ]);

        if ($this->is_email($request->username)) {
            $user = User::where('email', $request->username)->first();

            # check if user exist
            if (!$user) {
                throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No account with Email found');
            }

            # check if user already attempt forgot password process
            $old_forgot = Password::where('user_id', $user->id)->first();

            if ($old_forgot) {
                event(new ForgotPassword($old_forgot));

                return $this->success('Reset password proccess was initaited, check your email for the instructions', 200);
            }

            $forgot = Password::create([
                'user_id' => $user->id,
                'code' => mt_rand(193721, 999999),
            ]);

            if (!$forgot) {
                throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to initaite the reset password process');
            }

            $forgot->url = env('APP_EXTERNAL_DOMAIN') . 'reset-password?code=' . $forgot->code;

            $forgot->save();

            event(new ForgotPassword($forgot));

            return $this->success('Reset password proccess initaited, check your email for the instructions', 200);
        }

        $user = User::where('username', $request->username)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No account with Username found');
        }

        # check if user already attempt forgot password process
        $old_forgot = Password::where('user_id', $user->id)->first();

        if ($old_forgot) {
            event(new ForgotPassword($old_forgot));

            return $this->success('Reset password proccess was initaited, check your email for the instructions', 200);
        }

        $forgot = Password::create([
            'user_id' => $user->id,
            'code' => mt_rand(193721, 999999),
        ]);

        if (!$forgot) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to initaite the reset password process');
        }

        $forgot->url = env('APP_EXTERNAL_DOMAIN') . 'reset-password?code=' . $forgot->code;

        $forgot->save();

        event(new ForgotPassword($forgot));

        return $this->success('reset password proccess initaited, check your email for the instructions');
    }

    public function verify_reset_code(Request $request)
    {
        $this->validate($request, [
            'code' => 'bail|required|string'
        ]);

        $reset = Password::where('code', $request->code)->first();

        if (!$reset) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Code did not match any account');
        }

        return $this->success('Enter new password');
    }

    public function reset_password(Request $request)
    {
        $this->validate($request, [
            'code' => 'bail|required', 
            'password' => 'bail|required|string|confirmed'
        ]);

        $reset_password = Password::where('code', $request->code)->first();

        $user = User::where('id', $reset_password->user_id)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No user account was found');
        }

        $user->password = Hash::make($request->input('password'));

        if (!$user->save()) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Something went wrong when trying to reset password');
        }

        $reset_password->delete();

        event(new ResetPassword($user));

        return $this->success('Password resetted');
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $user = $request->user();

        return $this->response->item($user, (new UserTransformer)->setDefaultIncludes([]));
    }

    public function change_password(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'bail|required|string',
            'password' => 'bail|required|string|min:6|confirmed'
        ]);

        $user = $request->user();

        if (!Hash::check($request->old_password, $user->password)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Old password didn\'t match');
        }

        $result = User::where('id', $user->id)->update([
            'password' => Hash::make($request->password)
        ]);

        if (!$result) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to change password user account');
        }

        return $this->success('Account password has been changed, we will log you out so you can login with new password');
    }

    public function change_username(Request $request)
    {
        $this->validate($request, [
            'username' => 'bail|required|string|unique:users'
        ]);

        $user = $request->user();

        $user->username = $request->username;

        if (!$user) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to change account username');
        }

        // send email
        return $this->success('Account username changed');
    }

    /**
     * Update profile.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_profile(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|string',
            'location' => 'bail|required|string',
            'bio' => 'bail|required|string',
            'gender' => 'bail|required|string'
        ]);

        $user = $request->user();

        $result = User::where('id', $user->id)->update([
            'name' => $request->name,
            'bio' => $request->bio,
            'gender' => $request->gender,
            'location' => $request->location,
            'phone_number' => $request->phone_number,
            'dob' => $request->dob
        ]);

        if (!$result) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update account!!!');
        }

        return $this->success('Updated account');

    }

    /**
     * Update profile picture.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_profile_picture(Request $request)
    {
        $this->validate($request, [
            'file' => 'bail|required|mimes:jpg,png,gif,jpeg'
        ]);

        $user = $request->user();

        if (isset($request->file)) {
            $picture_location = (isset($request->file)) ? $request->file->storeAs('users', str_slug($user->name) . '.' . $request->file('file')->getClientOriginalExtension(), 'public') : '';
        } else {
            $picture_location = $user->picture;
        }

        $user->picture = $picture_location;
        $result = $user->save();

        if (!$result) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update account avatar!!!');
        }

        return $this->success('Updated account avatar');

    }

    /**
     * Get all playlist.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_all_playlists(Request $request, $pagination = 20)
    {
        if ($request->exists('name')) {
            if (!empty($request->name)) {
                $paginator = Playlist::where('name', $request->name)->paginate($pagination);
            } else {
                $paginator = Playlist::paginate($pagination);
            }
        } else {
            $paginator = Playlist::paginate($pagination);
        }

        // Get transformed array of data
        return $this->response->paginator($paginator, new PlaylistTransformer);
    }

    /**
     * Create playlist.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create_playlist(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|string|unique:playlists',
            'description' => 'bail|required|string',
            'picture' => 'bail|required|mimes:jpg,png,gif,jpeg'
        ]);

        $picture_location = (isset($request->picture)) ? $request->picture->storeAs('users/playlists', str_slug($request->name) . '.' . $request->file('picture')->getClientOriginalExtension(), 'public') : '';

        $user = $request->user();

        $data = Playlist::create([
            'user_id' => $user->id,
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'description' => $request->description,
            'picture' => $picture_location,
            'privacy' => $request->privacy,
        ]);

        if (!$data) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to create playlist!!!');
            
        }

        return $this->success('New playlist created');

    }

    /**
     * Update playlist picture
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_playlist_picture(Request $request)
    {
        $this->validate($request, [
            'file' => 'bail|required|mimes:jpg,png,gif,jpeg'
        ]);

        $user = $this->guard('jwt-auth:api')->user();

        $playlist = Playlist::findOrFail($request->playlist_id);

        if (isset($request->file)) {
            Storage::disk('public')->delete($playlist->picture);
            $picture_location = (isset($request->file)) ? $request->file->storeAs('playlists', str_slug($playlist->name) . '.' . $request->file('file')->getClientOriginalExtension(), 'public') : '';
        } else {
            $picture_location = $playlist->picture;
        }

        $playlist->picture = $picture_location;
        $result = $playlist->save();

        if (!$result) {
            # error
            return $this->error('error updating playlist picture!!!', 302);
        }

        return $this->success('playlist picture updated', 200);

    }

    /**
     * Update playlist.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_playlist(Request $request, $playlist_id = '')
    {
        $this->validate($request, [
            'name' => 'bail|required|string',
            'description' => 'bail|required|string',
        ]);

        $user = $request->user();

        $playlist = Playlist::where('id', $playlist_id)->where('user_id', $user->id)->first();

        if (isset($request->picture)) {
            $picture_location = (isset($request->picture)) ? $request->picture->storeAs('playlists', str_slug($playlist->name) . '.' . $request->file('picture')->getClientOriginalExtension(), 'public') : '';
        } else {
            $picture_location = $playlist->picture;
        }

        $result = Playlist::where('id', $playlist->id)->update([
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'description' => $request->description,
            'privacy' => $request->privacy,
            'picture' => $picture_location
        ]);

        if (!$result) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update playlist!!!');
        }

        return $this->success('Updated playlist');

    }

    public function delete_playlist(Request $request, $id='')
    {
        $playlist = Playlist::findOrFail($id);
        $file = $playlist->picture;
        if (!$playlist->delete()) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to delete playlist!!!');
        }
        Storage::disk('public')->delete($file);

        return $this->success('Deleted playlist');
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function logout()
    {
        auth()->logout();

        return $this->success('Successfully logged out');
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function delete_account(Request $request)
    {
        $user = $request->user();
        $user->delete();
        auth()->logout();

        return $this->success('Account deleted');
    }

    /**
     * Follow artist profile.
     * @param \Illuminate\Http\Request $request, $profile_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function follow_profile(Request $request, $profile_id = '')
    {
        $user = $request->user();

        $follower = Following::where('user_id', $user->id)->where('profile_id', $profile_id)->first();

        if ($follower) {
            return $this->success('You already followed the profile');
        }

        $query = $user->follow_profile($user->id, $request->profile_id);

        if (!$query) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to follow profile!!!');
        }

        return $this->success('You followed a profile');
    }

    /**
     * Like An Album.
     * @param \Illuminate\Http\Request $request, $album_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function like_album(Request $request, $album_id = '')
    {
        $user = $this->guard('auth:api')->user();

        $already_like = Like::where('user_id', $user->id)->where('likeable_id', $album_id)->where('likeable_type', 'App\Album')->first();

        if ($already_like) {
            return $this->success('you already like the album');
        }

        $album = Album::where('id', $album_id)->first();

        if (!$album) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Album does not exist');
        }

        $query = $user->like_album($user->id, $album_id);

        if (!$query) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to like album!!!');
        }

        return $this->success('You liked an ablum');
    }

    /**
     * Review An Album.
     * @param \Illuminate\Http\Request $request, $album_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function review_album(Request $request, $album_id = '')
    {
        $this->validate($request, [
            'review' => 'bail|required|string|min:10'
        ]);

        $user = $request->user();

        $album = Album::where('id', $album_id)->first();

        if (!$album) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Album does not exist');
        }

        $review = new Review();
        $review->user_id = $user->id;
        $review->content = $request->review;

        $review_album = $album->reviews()->save($review);

        if (!$review_album) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to review album');
        }

        return $this->success('You reviewed an album');
    }

}
