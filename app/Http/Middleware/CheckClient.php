<?php

namespace App\Http\Middleware;

use Closure;

class CheckClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($_SERVER['HTTP_CLIENT_TYPE'])){  
            return response()->json(['message' => 'Unauthorized: no client type'], 401);  
        }

        $response = $next($request);

        // Post-Middleware Action

        return $response;
    }
}
