<?php

namespace App\Transformers;

use App\Models\Blog;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class BlogTransformer extends TransformerAbstract
{
	protected $availableIncludes = [
        'category', 'related', 'comments'
    ];

	/**
	* Transform a Blog model into an array
	*
	* @param Blog $blog
	* @return array
	*/
	public function transform(Blog $blog)
	{
		return [
			'id' => (int) $blog->id,
			'title' => $blog->title,
			'video' => $blog->video_id,
			'description' => $blog->content,
			'slug' => $blog->slug,
			'tag' => $blog->tags,
			'seoTitle' => $blog->seo_title,
			'seoKeywords' => $blog->seo_keywords,
			'seoDescription' => $blog->seo_description,
			'view' => $blog->views,
			'total_comment' => $blog->comments->count(),
			'promote' => $blog->is_promoted,
			'readmore' => Str::words($blog->content, $words = 50, $end = '...'),
			'created' => Carbon::parse($blog->created_at)->diffForHumans(),
			// 'updated' => Carbon::parse($blog->updated_at)->toIso8601String(),
			'pic' => $blog->picture,
		];
	}

	public function includeCategory(Blog $blog)
    {
    	$category = $blog->category;

        return $this->item($category, new CategoryTransformer);
    }

	public function includeComments(Blog $blog)
    {
    	$comments = $blog->comments;

        return $this->collection($comments, new CommentTransformer);
    }

	public function includeRelated(Blog $blog)
    {
    	$related = $blog->category->blogs()->limit(3)->get();

        return $this->collection($related, new BlogTransformer);
    }
}