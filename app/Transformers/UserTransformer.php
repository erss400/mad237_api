<?php

namespace App\Transformers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
	protected $defaultIncludes = [
        'playlists'
    ];

	/**
	* Transform a User model into an array
	*
	* @param User $user
	* @return array
	*/
	public function transform(User $user)
	{
		return [
			'id' => (int) $user->id,
			'name' => $user->name,
			'username' => $user->username,
			'date_of_birth' => $user->dob,
			'gender' => $user->gender,
			'pic' => asset('storage/' . 'avatar.svg'),
			// 'pic' => ($user->picture === null) ? asset('storage/avatar.svg') : asset('storage/'. $user->picture),
			'bio' => $user->bio,
			'location' => $user->location,
			'phoneNumber' => $user->phone_number,
			'created' => Carbon::parse($user->created_at)->diffForHumans(),
		];
	}

	public function includePlaylists(User $user)
    {
    	$playlists = $user->playlists;

        return $this->collection($playlists, new PlaylistTransformer);
    }
}