<?php

namespace App\Transformers;

use App\Models\Album;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class AlbumTransformer extends TransformerAbstract
{
	/**
     * List of resources to automatically include
     *
     * @var array
     */

	// $defaultIncludes

    protected $availableIncludes = [
        'songs', 'profile', 'producer', 'reviews'
    ];

	/**
	* Transform a Album model into an array
	*
	* @param Album $album
	* @return array
	*/
	public function transform(Album $album)
	{
		return [
			'id' => (int) $album->id,
			'title' => $album->title,
			'slug' => $album->slug,
			'description' => $album->description,
			'total_songs' => $album->songs->count(),
			'type' => $album->type,
			'release_date' => $album->release_date,
			'created' => Carbon::parse($album->created_at)->toIso8601String(),
			'promote' => $album->is_promoted,
			'pic' => asset('storage/'. $album->picture),
            'likes_count' => count($album->likes),
			// 'updated' => Carbon::parse($album->updated_at)->toIso8601String(),
		];
	}

	public function includeProfile(Album $album)
    {
    	$profile = $album->profile;

        if (!$profile) {
            return null;
        }

        return $this->item($profile, new ProfileTransformer);
    }

	public function includeProducer(Album $album)
    {
    	$producer = $album->producer;

        if (!$producer) {
            return null;
        }

        return $this->item($producer, new ProfileTransformer);
    }

	/**
     * Include Songs
     *
     * @param Album $album
     * @return \League\Fractal\Resource\Collection
     */
    public function includeSongs(Album $album)
    {
        $songs = $album->songs;

        return $this->collection($songs, new SongTransformer);
    }

    /**
     * Include Reviews
     *
     * @param Album $album
     * @return \League\Fractal\Resource\Collection
     */
    public function includeReviews(Album $album)
    {
        $reviews = $album->reviews;

        return $this->collection($reviews, new ReviewTransformer);
    }
}