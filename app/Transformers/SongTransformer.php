<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\Song;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class SongTransformer extends TransformerAbstract
{
	protected $defaultIncludes = [
        'profile', 'album', 'producer', 'genre'
    ];

	/**
	* Transform a Song model into an array
	*
	* @param Song $song
	* @return array
	*/
	public function transform(Song $song)
	{
		return [
			'id' => (int) $song->id,
			'title' => $song->title,
			'artist' => $song->profile->stage_name,
			'slug' => $song->slug,
			'description' => $song->description,
			'readmore' => Str::words($song->description, $words = 50, $end = '...'),
			'created' => Carbon::parse($song->created_at)->diffForHumans(),
			'release_date' => $song->release_date,
			'view' => $song->views,
			'promote' => $song->is_promoted,
			'feature' => $song->feature,
			'pic' => asset('storage/'. $song->picture),
			'src' => asset('storage/'. $song->preview),
			'file' => asset('storage/'. $song->file),
		];
	}

	public function includeProfile(Song $song)
    {
    	$profile = $song->profile;

        return $this->item($profile, new ProfileTransformer);
    }

	public function includeGenre(Song $song)
    {
    	$genre = $song->genre;

        return $this->item($genre, new GenreTransformer);
    }

	public function includeAlbum(Song $song)
    {
    	$album = $song->album;

        return $this->item($album, new AlbumTransformer);
    }

	public function includeProducer(Song $song)
    {
    	$producer = $song->producer;

        return $this->item($producer, new ProfileTransformer);
    }
}