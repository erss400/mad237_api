<?php

namespace App\Transformers;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
	protected $defaultIncludes = [
        'profile'
    ];

	protected $availableIncludes = [
        'pictures', 'reviews',  'related'
    ];

	/**
	* Transform a Product model into an array
	*
	* @param Product $product
	* @return array
	*/
	public function transform(Product $product)
	{
		return [
			'id' => (int) $product->id,
			'name' => $product->title,
			'slug' => $product->slug,
			'size' => explode(',', $product->sizes),
			'color' => explode(',', $product->colors),
			'unq_code' => $product->code,
			'price' => $product->price,
			'inventory' => $product->inventory,
			'canShip' => $product->can_ship,
			'discount' => $product->discount,
			'description' => $product->description,
			'distribution_center' => $product->distribution_center,
			'promote' => $product->is_promoted,
			'readmore' => Str::words($product->description, $words = 50, $end = '...'),
			'created' => Carbon::parse($product->created_at)->toIso8601String(),
			// 'updated' => Carbon::parse($product->updated_at)->toIso8601String(),
			'pic' => asset('storage/' . $product->picture),
			'total_review' => $product->reviews->count(),
		];
	}

	public function transform_color($data)
	{
		$e_data = explode(',', $data);

		$result = '';

		foreach ($e_data as $value) {
			$result .= '"'. $value .'",';
		}

		$result = rtrim($result, ',');

		return '[' . $result . ']';
	}

	public function includePictures(Product $product)
    {
    	$pictures = $product->pictures;

        return $this->collection($pictures, new PictureTransformer);
    }

	public function includeProfile(Product $product)
    {
    	$profile = $product->profile;

        return $this->item($profile, new ProfileTransformer);
    }

    public function includeReviews(Product $product)
    {
    	$reviews = $product->reviews;

        return $this->collection($reviews, new ReviewTransformer);
    }

	public function includeRelated(Product $product)
    {
    	$related = $product->profile->products()->where('inventory', '>', 0)->where('id', '!==', $product->id)->limit(3)->get();

        return $this->collection($related, new ProductTransformer);
    }
}