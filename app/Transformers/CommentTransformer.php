<?php

namespace App\Transformers;

use App\Models\Blog;
use App\Models\Comment;
use Carbon\Carbon;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
	protected $availableIncludes = [
        'blog'
    ];

	/**
	* Transform a Comment model into an array
	*
	* @param Comment $comment
	* @return array
	*/
	public function transform(Comment $comment)
	{
		return [
			'id' => (int) $comment->id,
			'name' => $comment->name,
			// 'email' => $comment->email,
			'comment' => $comment->content,
			'created' => Carbon::parse($comment->created_at)->diffForHumans(),
			// 'updated' => Carbon::parse($comment->updated_at)->toIso8601String(),
		];
	}

	public function includeBlog(Comment $comment)
    {
    	$blog = $comment->blog;

        return $this->item($blog, new BlogTransformer);
    }
}