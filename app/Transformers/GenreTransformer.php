<?php

namespace App\Transformers;

use App\Models\Genre;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class GenreTransformer extends TransformerAbstract
{
	/**
	* Transform a Genre model into an array
	*
	* @param Genre $slide
	* @return array
	*/
	public function transform(Genre $genre)
	{
		return [
			'id' => (int) $genre->id,
			'title' => $genre->name,
			'slug' => $genre->slug,
			'description' => $genre->description,
			'created' => Carbon::parse($genre->created_at)->toIso8601String(),
			// 'updated' => Carbon::parse($slide->updated_at)->toIso8601String(),
		];
	}
}