<?php

namespace App\Transformers;

use App\Models\Profile;
use Carbon\Carbon;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class ProfileTransformer extends TransformerAbstract
{
	protected $availableIncludes = [
        'genre', 'products', 'photos', 'songs', 'videos', 'albums', 'related'
    ];

	/**
	* Transform a Profile model into an array
	*
	* @param Profile $profile
	* @return array
	*/
	public function transform(Profile $profile)
	{
		return [
			'id' => (int) $profile->id,
			'name' => $profile->name,
			'stageName' => $profile->stage_name,
			'email' => $profile->email,
			'phone' => $profile->phone,
			'location' => $profile->location,
			'date_of_birth' => $profile->dob,
			'gender' => $profile->gender,
			'bio' => $profile->content,
			'book' => $profile->booking,
			'manager_data' => $profile->manager_content,
			'label_data' => $profile->label_content,
			'sign' => $profile->is_sign,
			'slug' => $profile->slug,
			'facebook' => $profile->facebook,
			'instagram' => $profile->instagram,
			'twitter' => $profile->twitter,
			'view' => $profile->views,
			'promote' => $profile->is_promoted,
			'type' => $profile->type,
			'readmore' => Str::words($profile->content, $words = 50, $end = '...'),
			'created' => Carbon::parse($profile->created_at)->toIso8601String(),
			// 'updated' => Carbon::parse($profile->updated_at)->toIso8601String(),
			'pic' => asset('storage/'. $profile->picture),
			'logo' => asset('storage/'. $profile->logo),
			'banner' => asset('storage/'. $profile->banner),
		];
	}

	public function includeGenre(Profile $profile)
    {
    	$genre = $profile->genre;

        return $this->item($genre, new GenreTransformer);
    }

	public function includeProducts(Profile $profile)
    {
    	$products = $profile->products;

        return $this->collection($products, new ProductTransformer);
    }

	public function includeAlbums(Profile $profile)
    {
    	$albums = $profile->albums;

        return $this->collection($albums, new AlbumTransformer);
    }

	public function includeSongs(Profile $profile)
    {
    	$songs = $profile->songs;

        return $this->collection($songs, new SongTransformer);
    }

	public function includeVideos(Profile $profile)
    {
    	$videos = $profile->videos;

        return $this->collection($videos, new VideoTransformer);
    }

	public function includePhotos(Profile $profile)
    {
    	$photos = $profile->photos;

        return $this->collection($photos, new PhotoTransformer);
    }

	public function includeRelated(Profile $profile)
    {
    	$related = $profile->genre->profiles()->where('type', 'artist')->limit(3)->get();

        return $this->collection($related, new ProfileTransformer);
    }
}