<?php

namespace App\Transformers;

use App\Models\Blog;
use App\Models\Review;
use Carbon\Carbon;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class ReviewTransformer extends TransformerAbstract
{
	protected $availableIncludes = [
        'parent'
    ];

    protected $defaultIncludes = [
    	'user'
    ];

	/**
	* Transform a Review model into an array
	*
	* @param Review $review
	* @return array
	*/
	public function transform(Review $review)
	{
		return [
			'id' => (int) $review->id,
            'name' => $review->name,
            'email' => $review->email,
			'review' => $review->content,
			'created' => Carbon::parse($review->created_at)->diffForHumans(),
		];
	}

	public function includeParent(Review $review)
    {
    	if ($review->reviewable_type == 'App\Models\Product') {
    		$product = $review->reviewable;

        	return $this->item($product, new ProductTransformer);
    	} elseif ($review->reviewable_type == 'App\Models\Album') {
    		$album = $review->reviewable;

    		return $this->item($album, new AlbumTransformer);
    	}
    }

    public function includeUser(Review $review)
    {
    	if ($review->reviewable_type == 'App\Models\Product') {
            return null;
        }

        $user = $review->user;

        return $this->item($user, new UserTransformer);
    }
}