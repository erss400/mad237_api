<?php

namespace App\Transformers;

use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class VideoTransformer extends TransformerAbstract
{
	protected $defaultIncludes = [
        'profile', 'album', 'producer', 'genre'
    ];

	/**
	* Transform a Video model into an array
	*
	* @param Video $video
	* @return array
	*/
	public function transform(Video $video)
	{
		return [
			'id' => (int) $video->id,
			'link' => $video->link,
			'created' => Carbon::parse($video->created_at)->diffForHumans(),
		];
	}

	public function includeProfile(Video $video)
    {
    	$profile = $video->profile;

        return $this->item($profile, new ProfileTransformer);
    }
}