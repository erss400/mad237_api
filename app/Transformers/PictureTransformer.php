<?php

namespace App\Transformers;

use App\Models\Picture;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class PictureTransformer extends TransformerAbstract
{
	protected $availableIncludes = [
        'product'
    ];
	/**
	* Transform a Picture model into an array
	*
	* @param Picture $picture
	* @return array
	*/
	public function transform(Picture $picture)
	{
		return [
			'id' => (int) $picture->id,
			'pic' => asset('storage/'. $picture->picture),
		];
	}

	public function includeProduct(Picture $picture)
    {
    	$product = $picture->product;

        return $this->item($product, new ProfileTransformer);
    }
}