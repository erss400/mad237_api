<?php

namespace App\Transformers;

use App\Models\Photo;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class PhotoTransformer extends TransformerAbstract
{
	protected $availableIncludes = [
        'profile'
    ];
	/**
	* Transform a Photo model into an array
	*
	* @param Photo $photo
	* @return array
	*/
	public function transform(Photo $photo)
	{
		return [
			'id' => (int) $photo->id,
			'pic' => asset('storage/'. $photo->picture),
		];
	}

	public function includeProfile(Photo $photo)
    {
    	$profile = $photo->profile;

        return $this->item($profile, new ProfileTransformer);
    }
}