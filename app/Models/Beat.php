<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Beat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'profile_id', 'genre_id', 'description', 'preview', 'url', 'price', 'code', 'picture', 'tags', 'bpm', 'views', 'promoted'
    ];

    /**
     * Get the genre that owns the beat.
     */
    public function genre()
    {
        return $this->belongsTo('App\Models\Genre');
    }

    /**
     * Get the producer that owns the beat.
     */
    public function profile()
    {
        return $this->belongsTo('App\Models\Profile');
    }

    /**
     * Get all of the beat's reviews.
     */
    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable');
    }
}
