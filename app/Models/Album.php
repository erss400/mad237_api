<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'profile_id', 'description', 'picture', 'release_date', 'producer_id'
    ];

    /**
     * Get the artist that owns the audio.
     */
    public function profile()
    {
        return $this->belongsTo('App\Models\Profile');
    }

    /**
     * Get the producer of the model
     */
    public function producer()
    {
        return $this->belongsTo('App\Models\Profile');
    }

    /**
     * Get the songs for the model.
     */
    public function songs()
    {
        return $this->hasMany('App\Models\Song');
    }

    /**
     * Get all of the album reviews.
     */
    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable');
    }

    /**
     * Get all of the album likes.
     */
    public function likes()
    {
        return $this->morphMany('App\Models\Like', 'likeable');
    }
}
