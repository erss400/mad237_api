<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'app_name', 'logo', 'favicon', 'currency', 'about', 'phone', 'email', 'facebook', 'instagram', 'youtube', 'twitter', 'seo_title', 'seo_keywords', 'seo_description',
    ];
}
