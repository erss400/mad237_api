<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'stage_name', 'slug', 'gender', 'picture', 'is_sign', 'label_content', 'facebook', 'twitter', 'instagram', 'manager_content', 'genre_id', 'content', 'views', 'promoted', 'logo', 'banner', 'booking', 'email', 'phone', 'location'
    ];

    public function genre()
    {
    	return $this->belongsTo('App\Models\Genre');
    }

    /**
     * Get the albums for the model.
     */
    public function albums()
    {
        return $this->hasMany('App\Models\Album');
    }

    /**
     * Get the songs for the model.
     */
    public function songs()
    {
        return $this->hasMany('App\Models\Song');
    }

    /**
     * Get the videos for the model.
     */
    public function videos()
    {
        return $this->hasMany('App\Models\Video');
    }

    /**
     * Get the products for the model.
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

   /**
     * Get photos of the model.
    */
    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }
}
