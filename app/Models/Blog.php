<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'picture', 'video_id', 'category_id',  'tags', 'content', 'seo_title', 'seo_keywords', 'seo_description',
    ];

    public function category()
    {
    	return $this->belongsTo('App\Models\Category');
    }

    /**
     * Get all of the artist's reviews.
     */
    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable');
    }

    /**
     * Get all of the blog comments.
     */
    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    // public function getCreatedAtAttribute($value)
    // {
    //     return Carbon::parse($value)->diffForHumans();
    // }
}
