<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['namespace' => 'App\Http\Controllers\Api\V1'], function ($api)
	{
		$api->get('country/search', 'SearchController@search_country');

		$api->get('state/search', 'SearchController@search_state');

		$api->get('city/search', 'SearchController@search_city');

		$api->get('category/search', 'SearchController@search_category');

		$api->get('get_slides', 'SlideController@get_slides');

		$api->post('save_beat_data', 'WelcomeController@save_beat');

		$api->get('get_latest_profiles', 'LatestController@get_profiles');

		$api->get('get_latest_blogs', 'LatestController@get_blogs');

		$api->get('get_latest_songs', 'LatestController@get_songs');

		$api->get('get_profiles', 'ProfileController@get_profiles');

		$api->get('get_profile_by_slug/{slug}', 'ProfileController@get_profile_by_slug');

		$api->get('get_albums', 'AlbumController@get_albums');

		$api->get('get_album_by_slug/{slug}', 'AlbumController@get_album_by_slug');

		$api->get('get_songs', 'AlbumController@get_songs');

		$api->get('get_song_by_id/{id}', 'AlbumController@get_song_by_id');

		$api->get('get_videos', 'AlbumController@get_videos');

		$api->get('get_video_by_id/{id}', 'AlbumController@get_video_by_id');

		$api->get('get_beats', 'BeatController@get_beats');

		$api->get('get_blogs', 'BlogController@get_blogs');

		$api->get('get_blog_by_slug/{slug}', 'BlogController@get_blog_by_slug');

		$api->get('get_categories', 'BlogController@get_categories');

		$api->get('get_category_by_slug/{slug}', 'BlogController@get_category_by_slug');

		$api->post('add_blog_comment', 'BlogController@add_blog_comment');

		$api->get('get_genres', 'ProfileController@get_genres');

		$api->get('get_products', 'ProductController@get_products');

		$api->get('get_product_by_slug/{slug}', 'ProductController@get_product_by_slug');

		$api->post('add_product_review', 'ProductController@add_product_review');

		$api->group(['prefix' => 'account'], function ($api)
		{
			$api->post('/register', 'AccountController@register');

			$api->patch('/verify/{code}', 'AccountController@verify');

			$api->post('/login', 'AccountController@login');

			$api->get('/logout', 'AccountController@logout');

			$api->get('/me', 'AccountController@me');

			$api->get('/refresh', 'AccountController@refresh');

			$api->post('/change_password', 'AccountController@change_password');

			$api->post('/update_profile', 'AccountController@update_profile');

			$api->post('/update_profile_picture', 'AccountController@update_profile_picture');

			$api->patch('/follow_profile/{profile_id}', 'AccountController@follow_profile');

			$api->patch('/like_album/{album_id}', 'AccountController@like_album');

			$api->patch('/review_album/{album_id}', 'AccountController@review_album');

			$api->get('/get_all_playlists', 'AccountController@get_all_playlists');

			$api->get('/get_albums/{user_id}', 'AccountController@get_albums');

			$api->post('/create_playlist', 'AccountController@create_playlist');

			$api->post('/update_playlist', 'AccountController@update_playlist');

			$api->delete('/delete_playlist/{id}', 'AccountController@delete_playlist');

			$api->post('/update_picture', 'AccountController@update_picture');

			$api->delete('/delete', 'AccountController@delet_account');
		});
	});
});